boost_VER=1.81.0
_boost_VER=1_81_0

%/usr/include/boost: TMP_PATH=$*/tmp/
%/usr/include/boost: INSTALL_PATH=$*/

%/usr/include/boost:  %tmp/ %/usr/include/
	@echo "============================================================"
	@echo "Installing BOOST version ${boost_VER}"
	cd ${TMP_PATH} && \
		wget   https://archives.boost.io/release/${boost_VER}/source/boost_${_boost_VER}.tar.gz && \
		tar xzf boost_${_boost_VER}.tar.gz && \
		sudo cp -r boost_${_boost_VER} ${INSTALL_PATH}/usr/include
