#!/bin/bash

SRPM_NAME=protobuf-3.6.1-4.el7.src.rpm
#SRPM_SOURCE=https://cbs.centos.org/kojifiles/packages/protobuf/3.6.1/4.el7/src/${SRPM_NAME}
DESTINATION_PATH=/app

#script to build the rpms from the source rpm in different containers for different systems

#your system will need qemu and the following line to enable it for docker containers
#docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

yum -y update

#for the rpm tools
yum -y install yum-utils rpm-build wget python3 python2

#for proto buff stuff
yum -y install autoconf automake emacs libtool python3-devel python3-setuptools python2-devel python2-setuptools

#get and build the srpm
cd /root
#wget ${SRPM_SOURCE}
cp /app/${SRPM_NAME} ./
#TODO: figure out how to fix the output path of the source rpm.  Right now it is /root, though that is the systems second choice and the first one fails
rpm -i ${SRPM_NAME}
cd /root/rpmbuild/SPECS
#TODO: do some check for there being only one spec file and automatically choose that one
rpmbuild -ba protobuf.spec
#move to the mounted /app for the caller
cp -r /root/rpmbuild/RPMS /app
