SHELL=bash
SUB_VARIANT_SELECTION?=emmc
PWD=$(shell pwd)

SECURE_PATH=${PWD}/secure
SECURE_FILES="\
	secure/ssh/moduli \
	secure/ssh/ssh_config \
	secure/ssh/sshd_config \
	secure/ssh/ssh_host_ecdsa_key \
	secure/ssh/ssh_host_ecdsa_key.pub \
	secure/ssh/ssh_host_ed25519_key \
	secure/ssh/ssh_host_ed25519_key.pub \
	secure/ssh/ssh_host_rsa_key \
	secure/ssh/ssh_host_rsa_key.pub"

SECURE_CHECK=$(patsubst "secure",${SECURE_PATH},${SECURE_FILES})

%xc7z035.tar.xz : QEMU=qemu-arm-static
%xc7z045.tar.xz : QEMU=qemu-arm-static
%xczu7ev.tar.xz : QEMU=qemu-aarch64-static

%xc7z035.tar.xz : ZYNQ_ARCH=armv7hl
%xc7z045.tar.xz : ZYNQ_ARCH=armv7hl
%xczu7ev.tar.xz : ZYNQ_ARCH=aarch64

CONFIG_BASE_DIR=${PWD}/config
alma8_rev1_% : CONFIG_DIR=${CONFIG_BASE_DIR}/rev1
alma8_rev2_% : CONFIG_DIR=${CONFIG_BASE_DIR}/rev2
alma8_rev2a_% : CONFIG_DIR=${CONFIG_BASE_DIR}/rev2a
centos7_rev1_% : CONFIG_DIR=${CONFIG_BASE_DIR}/rev1
centos7_rev2_% : CONFIG_DIR=${CONFIG_BASE_DIR}/rev2
centos7_rev2a_% : CONFIG_DIR=${CONFIG_BASE_DIR}/rev2a

#OS build
alma8_% : OS=alma8
centos7_% : OS=centos7

#docker image
docker_alma% :   DOCKER_IMAGE=edfapollo/multiarch-butool-almalinux8:1.7-amd64
docker_centos% : DOCKER_IMAGE=edfapollo/multiarch-butool-centos7:1.7-amd64



INSTALL_BASE_PATH?=${PWD}/image/
%.tar.xz : INSTALL_PATH=${INSTALL_BASE_PATH}/$*/



SCRIPTS_PATH=${PWD}/scripts

CMS_UID=1000
CMS_GID=1000
ATLAS_UID=1001
ATLAS_GID=1001

ETC_PATH =${INSTALL_PATH}etc
HOME_PATH=${INSTALL_PATH}home
TMP_PATH =${INSTALL_PATH}tmp
OPT_PATH =${INSTALL_PATH}opt

APP_PATH=${PWD}

# File path for the version tracking file
VERSION_FILE_PATH=${ETC_PATH}/fs_version

all: centos7_rev1_xc7z035.tar.xz alma8_rev2_xczu7ev.tar.xz

.PHONEY: clean finalize_image all shell

shell_%xc7z035 : QEMU=qemu-arm-static
shell_%xc7z045 : QEMU=qemu-arm-static
shell_%xczu7ev : QEMU=qemu-aarch64-static
shell_%: INSTALL_PATH=${INSTALL_BASE_PATH}/$*/
shell_%:
	mount --bind /dev/ ./image/$*/dev/
	cp /etc/resolv.conf ./image/$*/etc/
	sudo chroot ${INSTALL_PATH} /${QEMU_PATH}/${QEMU}  /bin/bash
	rm ./image/$*/etc/resolv.conf
	umount ./image/$*/dev/

clean:
	rm -f *.yaml
	sudo rm -rf ${INSTALL_BASE_PATH}

mkdir_${INSTALL_BASE_PATH}/% :
	#Build output path
	rm -rf $*
	mkdir -p $*

${OPT_PATH}: ${TMP_PATH}
	sudo mkdir -p ${OPT_PATH}


include mk/QEMU.mk

include mk/pip.mk
include mk/uHAL.mk
include mk/CM_MCU.mk

include mk/inventory-monitor.mk
include mk/apollosm-inventory-monitor.mk
include mk/apollo-updater.mk
include mk/users.mk
include mk/boost.mk
include mk/extra_rpms.mk

#${SECURE_CHECK}:
#	$(error Missing secure files)

#
# Main Makefile rule to build the filesystem for rev1s and rev2s.
#
%.tar.xz :
	@echo "============================================================"
	@echo "QEMU:              ${QEMU}"
	@echo "ZYNQ_ARCH:         ${ZYNQ_ARCH}"
	@echo "CONFIG_BASE_DIR:   ${CONFIG_BASE_DIR}"
	@echo "CONFIG_DIR:        ${CONFIG_DIR}"
	@echo "OS:                ${OS}"
	@echo "DOCKER_IMAGE:      ${DOCKER_IMAGE}"
	@echo "INSTALL_BASE_PATH: ${INSTALL_BASE_PATH}"
	@echo "INSTALL_PATH:      ${INSTALL_PATH}"
	@echo "SCRIPTS_PATH:      ${SCRIPTS_PATH}"
	@echo "VARIANT:           ${SUB_VARIANT_SELECTION}"
	$(MAKE) ${INSTALL_PATH}${QEMU_PATH}/${QEMU} /${QEMU_PATH}/${QEMU}

	@echo "============================================================"
	@echo "Running mkrootfs script"
	cd ${OS}-rootfs && \
		sudo python3 mkrootfs.py --root=${INSTALL_PATH} --arch=${ZYNQ_ARCH} --extra=extra_rpms.txt

	@echo "============================================================"
	@echo "Applying common filesystem file mods "
	# Add common filesystem mods
	sudo cp -r ${CONFIG_BASE_DIR}/common/file_system/* ${INSTALL_PATH}

	# Choose the appropriate Variant for this installation "emmc" or "sd"
	sudo cp -r ${CONFIG_BASE_DIR}/common/file_system/etc/fstab_${SUB_VARIANT_SELECTION} ${INSTALL_PATH}/etc/fstab

	@echo "============================================================"
	@echo "Applying rev specific filesystem file mods ${CONFIG_DIR} "
	# Add rev specific mods
	sudo cp -r ${CONFIG_DIR}/file_system/* ${INSTALL_PATH}

	@echo "============================================================"
	@echo "Applying OS specific file system mods ${OS}"
	# Add OS specific mods
	sudo cp -r ${CONFIG_BASE_DIR}/${OS}/file_system/* ${INSTALL_PATH}

	#do any final updates
	sudo chmod o+x ${ETC_PATH}/rc.local

	@echo "============================================================"
	@echo "Adding version tracking file to FS"
	# Dump information about the repo from git
	echo "Filesystem version:" > ${VERSION_FILE_PATH}
	git config --global --add safe.directory ${APP_PATH}
	cd ${APP_PATH} && \
		git --no-pager show -s --format="Commit date: %ci  %d" >> ${VERSION_FILE_PATH}
	cd ${APP_PATH} && \
		git describe --dirty --always --tags --abbrev=14 >> ${VERSION_FILE_PATH}
	# Make the version file read-only
	sudo chmod 0444 ${VERSION_FILE_PATH}


	@echo "============================================================"
	@echo "Adding paths for FW"
	# Add the following dirs for the FW loading structure
	sudo mkdir -p ${INSTALL_PATH}/fw/SM
	sudo mkdir -p ${INSTALL_PATH}/fw/CM
	# Change ownership of the CM fw path
	sudo chmod a+rwX -R ${INSTALL_PATH}/fw/CM


	# Remove ipv6 from lighttpd
	sudo sed -i -e "s/server.use-ipv6 = \"enable\"/server.use-ipv6 = \"disable\"/g" ${ETC_PATH}/lighttpd/lighttpd.conf

	# This service breaks things
	sudo rm -f ${ETC_PATH}/systemd/system/multi-user.target.wants/auditd.service


	@echo "============================================================"
	@echo "Processing secure files"
	# Add all the password files and keys
	sudo install -m 600 -g 997 -o 0 ${SECURE_PATH}/ssh/ssh_host_ecdsa_key       ${ETC_PATH}/ssh/
	sudo install -m 600 -g 0   -o 0 ${SECURE_PATH}/ssh/ssh_host_ecdsa_key.pub   ${ETC_PATH}/ssh/
	sudo install -m 600 -g 997 -o 0 ${SECURE_PATH}/ssh/ssh_host_ed25519_key     ${ETC_PATH}/ssh/
	sudo install -m 600 -g 0   -o 0 ${SECURE_PATH}/ssh/ssh_host_ed25519_key.pub ${ETC_PATH}/ssh/
	sudo install -m 600 -g 997 -o 0 ${SECURE_PATH}/ssh/ssh_host_rsa_key         ${ETC_PATH}/ssh/
	sudo install -m 600 -g 0   -o 0 ${SECURE_PATH}/ssh/ssh_host_rsa_key.pub     ${ETC_PATH}/ssh/


	#add any non-repo rpms
	make extra_rpms_$*

	#install any pip packages requested
	${MAKE} pip_install_$*



	@echo "============================================================"
	@echo "Starting SW checkouts and builds"
	# Setup software and libraries
	${MAKE} ${OPT_PATH}/mcu_tools ${OPT_PATH}/inventory-monitor ${OPT_PATH}/apollosm-inventory-monitor ${OPT_PATH}/apollo-updater ${INSTALL_PATH}/usr/include/boost
	sudo cp ${SCRIPTS_PATH}/cleanup_locale.sh ${TMP_PATH}
	sudo rm -rf ${TMP_PATH}/*


	# Setup local user accounts
	${MAKE} users_$*

	@echo "============================================================"
	@echo "Building the final tarball"
	# Tar the whole filesystem
	cd ${INSTALL_PATH} && \
		tar --numeric-owner -p --xz -cf  ../$@ ./

#################################################################################
#docker builds
#################################################################################
docker_centos7 :
	docker pull ${DOCKER_IMAGE}
	docker run --rm --privileged -it -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v ${PWD}:/app:Z -v /etc/binfmt.d:/etc/binfmt.d:Z ${DOCKER_IMAGE}

docker_alma8 :
	docker pull ${DOCKER_IMAGE}
	docker run --rm --privileged -it -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v ${PWD}:/app:Z -v /etc/binfmt.d:/etc/binfmt.d:Z ${DOCKER_IMAGE}

docker_% :
	rm -f $*.tar.xz
	docker pull ${DOCKER_IMAGE}
	time docker run --rm --name os_build_$* --privileged -it -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v ${PWD}:/app:Z -v /etc/binfmt.d:/etc/binfmt.d:Z ${DOCKER_IMAGE} /app/scripts/docker_build.sh $*


#################################################################################
# Help
#################################################################################
#list magic: https://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile
list:
	@$(MAKE) -pRrq -f $(MAKEFILE_LIST) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]/]' -e '^$@$$' | column

