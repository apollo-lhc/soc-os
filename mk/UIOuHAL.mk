UIO_UHAL_URI=https://github.com/BU-Tools/UIOuHAL.git
UIO_UHAL_TAG=v2.0.0


#Makefile target-specific variables
%xc7z035/opt/cactus : QEMU=qemu-arm-static
%xc7z045/opt/cactus : QEMU=qemu-arm-static
%xczu7ev/opt/cactus : QEMU=qemu-aarch64-static

%opt/UIOuHAL: TMP_PATH=$*/tmp/

%opt/UIOuHAL: INSTALL_PATH=$*/

%opt/UIOuHAL: %opt/cactus
	echo ${TMP_PATH}
	cd ${TMP_PATH} && \
		git clone ${UIO_UHAL_URI} 
	cd ${TMP_PATH}/UIOuHAL && \
		git checkout ${UIO_UHAL_TAG}
	cp ${SCRIPTS_PATH}/build_uiouhal.sh ${TMP_PATH}/UIOuHAL
	sudo chroot ${INSTALL_PATH} ${QEMU_PATH}/${QEMU} /bin/bash /tmp/UIOuHAL/build_uiouhal.sh
