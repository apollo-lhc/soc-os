DTBO_TAG=v1.3.0
DTBO_URI=https://gitlab.com/apollo-lhc/soc-tools/bootup-daemon.git
NAME=uio-daemon

#BUILD based variables
%xc7z035/opt/${NAME} : QEMU=qemu-arm-static
%xc7z045/opt/${NAME} : QEMU=qemu-arm-static
%xczu7ev/opt/${NAME} : QEMU=qemu-aarch64-static


%/opt/${NAME}: OPT_PATH=$*/opt/

%/opt/${NAME}: %/opt/
	@echo "============================================================"
	@echo "Innstallingnn the UIO_daemon daemon and client"
	cd ${OPT_PATH} && \
		git clone --branch ${DTBO_TAG} ${DTBO_URI} ${NAME}
	cd ${OPT_PATH}/${NAME}/ && \
		make init
	sudo chroot ${OPT_PATH}/../ ${QEMU_PATH}/${QEMU} /bin/bash -c "cd /opt/${NAME} && make"
	sudo chroot ${OPT_PATH}/../ ${QEMU_PATH}/${QEMU} /bin/bash -c "cd /opt/${NAME} && make install"
#	ln -s /opt/${NAME}/uio_daemon.service $*/etc/systemd/system/uio_daemon.service
#	ln -s /opt/${NAME}/uio_daemon.service $*/etc/systemd/system/multi-user.target.wants/uio_daemon.service
