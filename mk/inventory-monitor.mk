INVENTORY_MONITOR_BRANCH=v0.4.1
INVENTORY_MONITOR_URI=https://gitlab.com/apollo-lhc/software/apollosm-inventory-monitor.git
INVENTORY_MONITOR_NAME=inventory-monitor

%/opt/${INVENTORY_MONITOR_NAME}: OPT_PATH=$*/opt/

%/opt/${INVENTORY_MONITOR_NAME}: %/opt/
	@echo "============================================================"
	@echo "Installing inventory-monitor daemon"
	cd ${OPT_PATH} && \
		git clone --branch ${INVENTORY_MONITOR_BRANCH} ${INVENTORY_MONITOR_URI} ${INVENTORY_MONITOR_NAME}
	ln -s /opt/${INVENTORY_MONITOR_NAME}/apollosm-inventory-monitor.service $*/etc/systemd/system/inventory_monitor.service
	ln -s /opt/${INVENTORY_MONITOR_NAME}/apollosm-inventory-monitor.service $*/etc/systemd/system/multi-user.target.wants/inventory_monitor.service

